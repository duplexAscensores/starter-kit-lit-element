import { html } from 'lit-html';
import '../packages/welcome-component';

export default {
  title: 'Welcome',
};

export const Welcome = () => html`
  <welcome-component></welcome-component>
`;
