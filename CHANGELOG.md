# [1.1.0](https://gitlab.com/duplexAscensores/starter-kit-lit-element/compare/v1.0.0...v1.1.0) (2020-03-20)


### Bug Fixes

* **gitlab-ci:** add script in job deploy ([ca47c58](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/ca47c5819e623bc789d047e507f4c247f9a04e3f))
* **gitlab-ci:** change dependencies job deploy ([bc82a70](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/bc82a70847d4b0f9986d549ed324312a763f6b95))
* **gitlab-ci:** change order stages ([6ffc3cc](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/6ffc3cc20a85657dbf8a4f8b9168c7d9431835f9))
* **gitlab-ci:** fix dependecies job karmatest ([abd50f3](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/abd50f34f779249a4fae779eac2c2011f45ffd17))


### Features

* ***:** new job test and new script package ([79bc71a](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/79bc71a3795f28a625d4011aae30b75ee7776d85))
* **gitlab-ci:** add job test in yamel ([1fee17d](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/1fee17db731dcdfb8404b8d8dee1a6ba9363e268))

# 1.0.0 (2020-03-20)


### Bug Fixes

* ***:** change preset angular ([52709d6](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/52709d66c7e112332154e3761e08e677b3c88426))
* ***:** change url yml ([af8d04c](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/af8d04c3df3661af522e0eda17a5ec5c662fa678))
* **gitlab:** remove option script semantic release ([61c5900](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/61c5900ae972fda52496a2a39e2774ab98a9d6be))
* **gitlab-ci:** add step before script ([a2ff023](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/a2ff02395c3becf69ec679ca4f1dad57223b5272))
* **gitlab-ci:** change before script ([c32b522](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/c32b5220cecfb185cf9c2a183a5b06bd0857a259))
* **gitlab-ci:** change gitlab_ssh_url ([632a29c](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/632a29c7927a54543acedd488b3c451ca679963e))
* **gitlab-ci:** change variables url ([63f04f6](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/63f04f6387e463e0b86296efea04895738ae94c1))
* **gitlab-ci:** change variables url ([08f4044](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/08f4044912a55b63c8e35a7a3fbf20915e742292))
* **releasconfig:** change url semantic release ([8f44809](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/8f44809c6fd8da5f63c84cf142f480bb8b82ea9a))


### Features

* ***:** init commit ([3dbc6e3](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/3dbc6e322a7a26a989d2e40f7627cdea354656eb))


* Add README.md ([8531eef](https://gitlab.com/duplexAscensores/starter-kit-lit-element/commit/8531eef69475ad83fd50a6c1e5bf2789d6a9bbde))
